package de.guchelkaben.mame.pokedeck.data.graphql

import de.guchelkaben.mame.pokedeck.data.Pokemon

data class RemoveObject(
        var length: Number = 0,
        var delObject : Pokemon? = null
)
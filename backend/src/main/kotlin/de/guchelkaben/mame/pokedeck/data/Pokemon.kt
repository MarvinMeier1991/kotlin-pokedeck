package de.guchelkaben.mame.pokedeck.data

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class Pokemon(
        @Id
        @GeneratedValue
        var id: Long = 0,
        var name: String = "",
        var type: PokemonType = PokemonType.NEUTRAL
) {
    constructor(name: String) : this() {
        this.name = name
    }
}
package de.guchelkaben.mame.pokedeck.service

import de.guchelkaben.mame.pokedeck.data.Pokemon
import de.guchelkaben.mame.pokedeck.repository.PokemonRepository
import org.springframework.stereotype.Service

@Service
class PokemonServiceImpl(private val pokemonRepository: PokemonRepository) : PokemonService {

    override fun findPokemonById(id: Long): Pokemon = pokemonRepository.findById(id).get()

    override fun savePokemon(pokemon: Pokemon): Pokemon = pokemonRepository.save(pokemon)

    override fun findAllPokemon(): MutableList<Pokemon> {
        val pokemonList: MutableList<Pokemon> = ArrayList()
        pokemonRepository.findAll().forEach { pokemonList.add(it) }
        return pokemonList
    }
}
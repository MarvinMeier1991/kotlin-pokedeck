package de.guchelkaben.mame.pokedeck.data

enum class PokemonType {
    NEUTRAL,
    WATER,
    FIRE,
    GRASS
}
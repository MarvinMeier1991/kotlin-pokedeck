package de.guchelkaben.mame.pokedeck.service

import de.guchelkaben.mame.pokedeck.service.datafetcher.mutation.CreatePokemonDataFetcher
import de.guchelkaben.mame.pokedeck.service.datafetcher.mutation.RemovePokemonDataFetcher
import de.guchelkaben.mame.pokedeck.service.datafetcher.query.AllPokemonDataFetcher
import de.guchelkaben.mame.pokedeck.service.datafetcher.query.PokemonDataFetcher
import graphql.GraphQL
import graphql.schema.GraphQLSchema
import graphql.schema.idl.RuntimeWiring
import graphql.schema.idl.SchemaGenerator
import graphql.schema.idl.SchemaParser
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.Resource
import org.springframework.stereotype.Service
import java.io.File
import javax.annotation.PostConstruct

@Service
class GraphQLService(
        private val allPokemonmDataFetcher: AllPokemonDataFetcher,
        private val pokemonmDataFetcher: PokemonDataFetcher,
        private val removePokemonDataFetcher: RemovePokemonDataFetcher,
        private val createPokemonDataFetcher: CreatePokemonDataFetcher) {

    @Value("classpath:pokemon.graphqls")
    private lateinit var resource: Resource

    lateinit var graphQL: GraphQL

    @PostConstruct
    fun loadGraphQLSchema() {

        val schemaFile: File = resource.file

        //Read schema file
        val typeRegistry = SchemaParser().parse(schemaFile)

        //"Gather" data logic for querying data
        val wiring: RuntimeWiring = buildRuntimeWiring()

        //wiring file with query definition
        val schema: GraphQLSchema = SchemaGenerator().makeExecutableSchema(typeRegistry, wiring)

        graphQL = GraphQL.newGraphQL(schema).build()

    }

    fun buildRuntimeWiring(): RuntimeWiring {
        return RuntimeWiring.newRuntimeWiring().type("Query") {
            it
                    .dataFetcher("allPokemons", allPokemonmDataFetcher)
                    .dataFetcher("pokemon", pokemonmDataFetcher)
        }.type("Mutation") {
            it
                    .dataFetcher("createPokemon", createPokemonDataFetcher)
                    .dataFetcher("removePokemon", removePokemonDataFetcher)
        }.build()
    }
}
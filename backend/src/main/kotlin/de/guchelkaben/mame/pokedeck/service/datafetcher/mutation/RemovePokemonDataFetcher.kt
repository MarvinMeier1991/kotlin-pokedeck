package de.guchelkaben.mame.pokedeck.service.datafetcher.mutation

import de.guchelkaben.mame.pokedeck.data.Pokemon
import de.guchelkaben.mame.pokedeck.data.graphql.RemoveObject
import de.guchelkaben.mame.pokedeck.repository.PokemonRepository
import graphql.schema.DataFetcher
import graphql.schema.DataFetchingEnvironment
import org.springframework.stereotype.Component

@Component
class RemovePokemonDataFetcher(private val pokemonRepository: PokemonRepository) : DataFetcher<RemoveObject> {

    override fun get(dataFetcher: DataFetchingEnvironment): RemoveObject {
        val id: String = dataFetcher.getArgument<String>("id")
        val pokemon: Pokemon = pokemonRepository.findById(id.toLong()).get()

        pokemon.let { pokemonRepository.delete(it) }

        return RemoveObject(1, pokemon)
    }
}
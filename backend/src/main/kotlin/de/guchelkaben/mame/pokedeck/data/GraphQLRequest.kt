package de.guchelkaben.mame.pokedeck.data


data class GraphQLRequest(
        var operationName: String = "",
        var variables: Map<String, Any>? = null,
        var query: String = ""
)
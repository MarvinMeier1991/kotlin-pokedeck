package de.guchelkaben.mame.pokedeck

import de.guchelkaben.mame.pokedeck.data.Pokemon
import de.guchelkaben.mame.pokedeck.data.PokemonType
import de.guchelkaben.mame.pokedeck.service.PokemonService
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PokedeckApplication(val pokemonService: PokemonService) : CommandLineRunner {

    override fun run(vararg args: String?) {

        val pokemon1: Pokemon = Pokemon()
        pokemon1.name = "Ivysaur"
        pokemon1.type = PokemonType.GRASS

        val pokemon2: Pokemon = Pokemon()
        pokemon2.name = "Charizard"
        pokemon2.type = PokemonType.FIRE


        val pokemon3: Pokemon = Pokemon()
        pokemon3.name = "Charmeleon"
        pokemon3.type = PokemonType.WATER

        pokemonService.savePokemon(pokemon1)
        pokemonService.savePokemon(pokemon2)
        pokemonService.savePokemon(pokemon3)
    }
}

fun main(args: Array<String>) {
    runApplication<PokedeckApplication>(*args)
}

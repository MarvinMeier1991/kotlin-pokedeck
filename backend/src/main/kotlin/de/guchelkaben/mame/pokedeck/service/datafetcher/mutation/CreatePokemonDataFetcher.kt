package de.guchelkaben.mame.pokedeck.service.datafetcher.mutation

import de.guchelkaben.mame.pokedeck.data.Pokemon
import de.guchelkaben.mame.pokedeck.repository.PokemonRepository
import graphql.schema.DataFetcher
import graphql.schema.DataFetchingEnvironment
import org.springframework.stereotype.Component

@Component
class CreatePokemonDataFetcher(private val pokemonRepository: PokemonRepository) : DataFetcher<Pokemon> {

    override fun get(dataFetcher: DataFetchingEnvironment): Pokemon {
        val name: String = dataFetcher.getArgument<String>("name")
        return pokemonRepository.save(Pokemon(name))
    }
}
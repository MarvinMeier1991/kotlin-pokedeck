package de.guchelkaben.mame.pokedeck.controller

import de.guchelkaben.mame.pokedeck.data.GraphQLRequest
import de.guchelkaben.mame.pokedeck.service.GraphQLService
import graphql.ExecutionInput
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/graphql/")
class GraphQLController(@Autowired val graphQLService: GraphQLService) {

    @PostMapping()
    fun graphQL(@RequestBody graphQLRequest: GraphQLRequest): ResponseEntity<Any> {
        return ResponseEntity.ok(graphQLService.graphQL.execute(ExecutionInput.newExecutionInput().query(graphQLRequest.query).variables(graphQLRequest.variables).operationName(graphQLRequest.operationName).build()))
    }
}
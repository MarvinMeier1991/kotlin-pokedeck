package de.guchelkaben.mame.pokedeck.repository

import de.guchelkaben.mame.pokedeck.data.Pokemon
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface PokemonRepository : CrudRepository<Pokemon, Long>
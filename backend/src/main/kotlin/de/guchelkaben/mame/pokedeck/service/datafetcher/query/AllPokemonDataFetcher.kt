package de.guchelkaben.mame.pokedeck.service.datafetcher.query

import de.guchelkaben.mame.pokedeck.data.Pokemon
import de.guchelkaben.mame.pokedeck.service.PokemonService
import graphql.schema.DataFetcher
import graphql.schema.DataFetchingEnvironment
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class AllPokemonDataFetcher(@Autowired val pokemonService : PokemonService) : DataFetcher<List<Pokemon>> {
    override fun get(dataFetchingEnvironment: DataFetchingEnvironment): List<Pokemon> = pokemonService.findAllPokemon()
}

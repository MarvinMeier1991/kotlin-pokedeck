package de.guchelkaben.mame.pokedeck.service

import de.guchelkaben.mame.pokedeck.data.Pokemon

interface PokemonService {
    fun findPokemonById(id: Long): Pokemon
    fun savePokemon(pokemon: Pokemon): Pokemon
    fun findAllPokemon(): MutableList<Pokemon>
}
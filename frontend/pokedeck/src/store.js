import Vue from 'vue';
import Vuex from 'vuex';
import {fetchPokemons, removePokemon} from "./components/pokemonService";
import _ from 'lodash';

Vue.use(Vuex);


export const store = new Vuex.Store({
    state: {
        pokemons: [
            {
                id: 1,
                name: 'Glurak',
                type: "FIRE"

            }
        ]
    },
    getters: {
        getPokemons: state => state.pokemons
    },
    mutations: {
        fetchPokemons(state, pokemons) {
            state.pokemons = pokemons;
        },
        removePokemon(state, pokemonId) {
            state.pokemons = _.remove(state.pokemons, (pokemon) => {
                return pokemon.id !== pokemonId
            });
        }
    },
    actions: {
        async fetchPokemons({commit}, vueIni) {
            let result = await fetchPokemons(vueIni);
            commit('fetchPokemons', result.data.allPokemons);
        },
        async removePokemon({commit}, pokemonId) {
            let result = await removePokemon(pokemonId);
            commit('removePokemon', pokemonId);
        }
    }
});

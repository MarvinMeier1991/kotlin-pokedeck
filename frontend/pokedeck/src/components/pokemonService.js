import {ALL_POKEMONS_QUERY, REMOVE_POKEMON_MUTATION} from '../graphql';

export const fetchPokemons = (vm) => {
    return vm.$root.$apollo.query({
        query: ALL_POKEMONS_QUERY
    });
};

export const removePokemon = (id) => {
    return window.vm.$root.$apollo.mutate({
        mutation: REMOVE_POKEMON_MUTATION,
        variables: {
            id
        }
    });
};

// export const REMOVE_POKEMON_MUTATION = gql`
//     mutation RemovePokemon($id: Int!) {
//         removePokemon(id: $id) {
//             length
//         }
//     }
// `;
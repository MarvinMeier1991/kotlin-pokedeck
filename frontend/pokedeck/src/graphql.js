import gql from 'graphql-tag';

export const ALL_POKEMONS_QUERY = gql`
    query AllPokemons {
        allPokemons {
            id
            name
            type
        }
    }
`;

export const REMOVE_POKEMON_MUTATION = gql`
    mutation RemovePokemon($id: ID!) {
        removePokemon(id: $id) {
            length
        }
    }
`;